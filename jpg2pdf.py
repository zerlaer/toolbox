import os

from PIL import Image


def openImage(jpgFile):
    path, fileName = jpgFile.rsplit('\\', 1)
    preName, postName = fileName.rsplit('.', 1)
    img = Image.open(jpgFile)
    return img.save(path + "\\" + preName + '.pdf', "PDF", resolution=100.0, save_all=True)


def convertImage(pathName):
    files = os.listdir(pathName)
    for f in files:
        if f.lower().find(".jpg") > 0:
            openImage(pathName + '\\' + f)


if __name__ == '__main__':
    convertImage(r'.')
