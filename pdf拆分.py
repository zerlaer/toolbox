import os
import PyPDF2
# pip install PyPDF2

def get_pdf():  # 获取并返回当前文件夹的所有PDF文件

    a = []
    for i in os.listdir():
        if '.pdf' in i or '.PDF' in i:
            a.append(i)
    return a


def main():
    for pdfs in get_pdf():
        pdf_file_address = pdfs
        pdf_name = os.path.splitext(pdf_file_address)
        input_pdf = PyPDF2.PdfFileReader(pdf_file_address)
        num_page = input_pdf.getNumPages()
        print('《' + pdf_file_address + '》共计' + str(num_page) + '页')
        print('正在拆分中')
        for i in range(0, num_page):
            page = input_pdf.getPage(i)
            output_pdf = PyPDF2.PdfFileWriter()
            output_pdf.addPage(page)
            output_pdf.write(open(pdf_name[0] + '第' + str(i + 1) + '页' + '.pdf', 'wb'))
    print('拆分完成')


if __name__ == '__main__':
    main()
