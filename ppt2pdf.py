import os
import comtypes.client
import win32api, win32con


def get_path():
    # 获取当前运行路径
    path = os.getcwd()
    # 获取所有文件名的列表
    filename_list = os.listdir(path)
    # 获取所有word文件名列表
    wordname_list = [filename for filename in filename_list if filename.endswith((".ppt", ".pptx"))]
    for pptname in wordname_list:
        # 分离word文件名称和后缀，转化为pdf名称
        pdfname = os.path.splitext(pptname)[0] + '.pdf'
        # 如果当前word文件对应的pdf文件存在，则不转化
        if pdfname in filename_list:
            continue
        # 拼接 路径和文件名
        pptpath = os.path.join(path, pptname)
        pdfpath = os.path.join(path, pdfname)
        # 生成器
        yield pptpath, pdfpath


def ppt_to_pdf():
    ppt = comtypes.client.CreateObject("Powerpoint.Application")
    ppt.Visible = 1
    for pptpath, pdfpath in get_path():
        newpdf = ppt.Presentations.Open(pptpath)
        newpdf.SaveAs(pdfpath, FileFormat=32)
        newpdf.Close()
        print(f'{pptpath} 已转换为 {pdfpath}')


if __name__ == "__main__":
    ppt_to_pdf()
