import os
import fitz
import os
# pip install python-pptx
import pptx

from pptx.util import Inches

pdf_dir = []


def pdf_to_png():
    documents = os.listdir()
    for document in documents:
        if os.path.splitext(document)[1] == '.pdf':  # 目录下包含.pdf的文件
            pdf_dir.append(document)
    for pdf in pdf_dir:
        doc = fitz.open(pdf)
        for pg in range(doc.pageCount):
            page = doc[pg]
            rotate = int(0)  # 页面旋转角度
            zoom_x = 5  # 修改该参数可以改变像素
            zoom_y = 5
            mat = fitz.Matrix(zoom_x, zoom_y).preRotate(rotate)
            pix = page.getPixmap(matrix=mat, alpha=False)
            pix.writePNG(f'{pg}.png')

    pptFile = pptx.Presentation()

    picFiles = [fn for fn in os.listdir() if fn.endswith('.png')]
    # 按图片编号顺序导入
    for fn in sorted(picFiles, key=lambda item: int(item[:item.rindex('.')])):
        slide = pptFile.slides.add_slide(pptFile.slide_layouts[1])
        # 为PPTX文件当前幻灯片中第一个文本框设置文字，本文代码中可忽略
        slide.shapes.placeholders[0].text = fn[:fn.rindex('.')]
        # 导入并为当前幻灯片添加图片，起始位置和尺寸可修改
        slide.shapes.add_picture(image_file=fn,
            left=Inches(0),
            top=Inches(0),
            width=Inches(10),
            height=Inches(7.5)
        )
    pptFile.save('output.pptx')
    for pngname in os.listdir():
        # 分离文件名与扩展名
        temp = os.path.splitext(pngname)
        # 因为python文件跟图片在同目录，所以需要判断一下
        if temp[1] == '.png':
            os.remove(pngname)


if __name__ == '__main__':
    pdf_to_png()
