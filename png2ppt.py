import os

import pptx

from pptx.util import Inches

pptFile = pptx.Presentation()

picFiles = [fn for fn in os.listdir() if fn.endswith('.png')]

# 按图片编号顺序导入

for fn in sorted(picFiles, key=lambda item: int(item[:item.rindex('.')])):
    slide = pptFile.slides.add_slide(pptFile.slide_layouts[1])
    # 为PPTX文件当前幻灯片中第一个文本框设置文字，本文代码中可忽略
    slide.shapes.placeholders[0].text = fn[:fn.rindex('.')]
    # 导入并为当前幻灯片添加图片，起始位置和尺寸可修改
    slide.shapes.add_picture(image_file=fn,
        left=Inches(0),
        top=Inches(0),
        width=Inches(12),
        height=Inches(7.5)
    )
pptFile.save('output.pptx')
