import os
# PIL : Python Imaging Library
from PIL import Image

# 获取目录下文件名
files = os.listdir()
# 图标大小
size = (64, 64)

# 给图标文件单独创建一个icon目录
if not os.path.exists('icon'):
    os.mkdir('icon')

for pngname in files:
    # 分离文件名与扩展名
    temp = os.path.splitext(pngname)
    # 因为python文件跟图片在同目录，所以需要判断一下
    if temp[1] == '.png':
        iconame = temp[0] + '.ico'
        # 打开图片并设置大小
        img = Image.open(pngname).resize(size)
        try:
            # 图标文件保存至icon目录
            path = os.path.join('icon', iconame)
            img.save(path)

            print(f'{pngname} 已转换为 {iconame}')
        except IOError:
            print('connot convert :', pngname)
