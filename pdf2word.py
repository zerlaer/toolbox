import os

from pdf2docx import Converter

def convter(pdf_file, word_file):
    conv = Converter(pdf_file)
    conv.convert(word_file)
    conv.close()


def pdf_to_word():
    file_list = [file for file in os.listdir() if os.path.splitext(file)[1] == '.pdf']
    for pdf in file_list:
        file_name = os.path.splitext(pdf)[0]
        word = file_name + ".docx"
        print("正在处理: ", pdf)
        convter(pdf, word)
    exit(0)


if __name__ == '__main__':
    pdf_to_word()
