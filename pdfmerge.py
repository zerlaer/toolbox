from PyPDF2 import PdfFileMerger
import os
# pip install PyPDF2
files = os.listdir()  # 列出目录中的所有文件

merge = PdfFileMerger()

for file in files:  # 从所有文件中选出pdf文件合并
    if file[-4:] == ".pdf":
        merge.append(open(file, 'rb'))
with open('output.pdf', 'wb') as output:  # 输出文件为output.pdf
    merge.write(output)
