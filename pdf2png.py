import os
import fitz
# pip install fitz PyMuPDF
pdf_dir = []


def get_file():
    documents = os.listdir()
    for document in documents:
        if os.path.splitext(document)[1] == '.pdf':  # 目录下包含.pdf的文件
            pdf_dir.append(document)
def conver_img():
    for pdf in pdf_dir:
        doc = fitz.open(pdf)
        for pg in range(doc.pageCount):
            page = doc[pg]
            rotate = int(0)  # 页面旋转角度
            zoom_x = 5  # 修改该参数可以改变像素
            zoom_y = 5
            mat = fitz.Matrix(zoom_x, zoom_y).preRotate(rotate)
            pix = page.getPixmap(matrix=mat, alpha=False)
            pix.writePNG(f'{pg}.png')


if __name__ == '__main__':
    get_file()
    conver_img()

